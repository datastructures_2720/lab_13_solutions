package objects;

import java.util.LinkedList;
import java.util.Queue;

import interfaces.IDTree;
import objects.DLine;

/**
 *This class should implement the interface <code>IDTree<DLine></code>.
 * <br>
 * <pre>
 * This class creates a binary (search) tree with nodes of type <code>DLine</code>. A DTree
 * follows these properties:
 * 		- It has a root DLine that is parent/ancestor of all nodes.
 * 		- For each DLine dl, the following conditions should always be held:
 * 			- dl.length > dl.leftChild.length
 * 			- dl.length <= dl.rightChild.length
 * </pre>
 * @author Azim Ahmadzadeh
 *
 */
public class DTree implements IDTree<DLine> {

	private DLine root;
	private int n;

	public DTree() {
		this.root = null;
		this.n = 0;
	}
	
	@Override
	public DLine getRoot() {
		return this.root;
	}
	
	@Override
	public int getNumberOfNodes() {

		return this.n;
	}

	@Override
	public void insert(DLine dl) {
		this.root = this.insertRec(this.root, dl);
		this.n++;
	}

	private DLine insertRec(DLine currentDLine, DLine dl) {

		// If this node does not exist
		if (currentDLine == null) {
			return new DLine(dl);
		}

		// If the new DLine is shorter than the current DLine,
		if (dl.length < currentDLine.length) {
			// add dl to the left of currentDLine
			currentDLine.setLeftChild(insertRec(currentDLine.getLeftChild(), dl));
		} else if (dl.length >= currentDLine.length) {
			// else, add dl to the right of currentDLine
			currentDLine.setRightChild(insertRec(currentDLine.getRightChild(), dl));
		}

		return currentDLine;
	}

	@Override
	public int getTreeLevel(DLine dl) {

		if (dl == null)
			return 0;

		int leftLevel = this.getTreeLevel(dl.getLeftChild());
		int rightLevel = this.getTreeLevel(dl.getRightChild());
		return Math.max(leftLevel, rightLevel) + 1;
	}
	
	@Override
	public void traverseInorder() {
		this.inorderRec(this.root);
	}
	
	private void inorderRec(DLine dl) {
		if(dl == null)
			return;
		
		this.inorderRec(dl.getLeftChild());
		System.out.print(dl.length + " ");
		this.inorderRec(dl.getRightChild());
	}
	
	@Override
	public void traversePreorder() {
		this.preorderRec(this.root);
	}
	
	private void preorderRec(DLine dl) {
		if(dl == null) {
			return;
		}
		System.out.print(dl.length + " ");
		this.preorderRec(dl.getLeftChild());
		this.preorderRec(dl.getRightChild());
	}
	
	@Override
	public void traversePostorder() {
		this.postorderRec(this.root);
	}
	
	private void postorderRec(DLine dl) {
		if(dl == null)  
			return;
		
		this.postorderRec(dl.getLeftChild());
		this.postorderRec(dl.getRightChild());
		System.out.print(dl.length + " ");
	}
	
	@Override
	public void traverseLevelOrder() {
		Queue<DLine> q = new LinkedList<DLine>();
		q.add(this.root);
		while (!q.isEmpty()) {
			DLine tmp = q.poll();
			System.out.print(tmp + " ");

			if (tmp.getLeftChild() != null)
				q.add(tmp.getLeftChild());

			if (tmp.getRightChild() != null) {
				q.add(tmp.getRightChild());
			}
		}
	}
	
	@Override
	public DLine find(DLine dl, int len) {
		if(dl == null) {
			return dl;
		}
		
		if(dl.length == len) {
			return dl;
		}
		if(dl.length > len)
			return(this.find(dl.getLeftChild(), len));
		
		return(this.find(dl.getRightChild(), len));
	}
	
}
